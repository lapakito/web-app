import colors from 'vuetify/es5/util/colors'

export default {
  ssr: false,
  head: {
    titleTemplate: '%s | lapakito.com',
    title: 'Welcome',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      {
        hid: 'icon',
        rel: 'icon',
        type: 'image/png',
        href: '/favicon.png',
      },
    ],
  },
  components: true,
  buildModules: ['@nuxtjs/vuetify'],
  vuetify: {
    treeShake: true,
    defaultAssets: {
      font: {
        family: 'Rubik',
      },
    },
    theme: {
      light: true,
      themes: {
        light: {
          satu: '#17C09E',
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },
  build: {},
  generate: {
    fallback: true,
  },
}
